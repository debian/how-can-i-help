how-can-i-help(1)
================
:doctype: manpage

NAME
----
how-can-i-help - show opportunities for contributing to Debian

SYNOPSIS
--------
*how-can-i-help* ['-haoqs'] ['--help'] ['--all'] ['--old'] ['--quiet'] ['--show <type>...']

DESCRIPTION
-----------
*how-can-i-help* hooks into APT to list opportunities for contributions to
Debian (orphaned packages, bugs tagged 'newcomer') for packages installed
locally, after each APT invocation. It can also be invoked directly, and
then lists all opportunities for contribution (not just the new ones).

OPTIONS
-------
Options must come before the other command line arguments.

*-h*, *--help*::
  Show some help.

*-a*, *--all*::
  Show new opportunities for contribution to all available Debian packages.

*-o*, *--old*::
  Show opportunities that were already shown before (will also show the new ones).

*-q*, *--quiet*::
  Do not display header and footer.

*-j*, *--json*::
  Display output in JSON format.

*-p*, *--apt*::
  Always exit with code 0. By default run by apt hook to prevent apt failures.

*-s <type>...*, *--show <type>...*::
  Show only specific types of opportunities. Provided types have to be separated by commas.

*--download*, *--no-download*::
  Enable or disable updating the cache when it is present.
  The cache is updated by default and when it is missing.

*--installed-packages*, *--no-installed-packages*::
  Enable or disable showing opportunities for packages installed locally.

*--autorm-days <N>*::
  Only show autoremovals within the next N days.

PROXY SUPPORT
-------------
*how-can-i-help* uses the HTTP_PROXY (or http_proxy) environment variable as
HTTP proxy configuration.

RUNNING ON REGULAR BASIS
------------------------
You can run *how-can-i-help* in a cron job as a normal user so that you
receive a mail when new things are reported. To receive only mails when new
contributions are reported, use the '-q' flag.

Crontab example:
-----
0 8 * * * how-can-i-help -q
-----

DISPLAY OUTPUT IN JSON FORMAT
-----------------------------
*how-can-i-help* can be configured to provide machine readable JSON output.
When running in this mode, only error messages and JSON output will be produced.
If there are no packages that would be shown, no output will be produced. Elements
in the JSON output are not sorted. All the other options can be used alongside
the '--json' option.

Simple example:
-----
how-can-i-help --json --show testing-autorm
-----

EXIT CODES
----------
When run by apt hook, *how-can-i-help* will always exit with code 0, even if an error
occurred. When *how-can-i-help* is called without '--apt' option, import and command
line parsing errors will still exit with code 0, but all the other errors will return
appropriate exit codes. Regardless of the '--apt' option, all error messages will
be displayed normally.

SHOW ONLY SPECIFIC TYPES OF OPPORTUNITIES
-----------------------------------------
*how-can-i-help* can be configured to show only specific types of opportunities.
You only need to run it with --show option followed by a single space and then
a list of types. Types have to be separated only by commas.

Simple example:
-----
how-can-i-help --show newcomer,RFH
-----

Example showing newcomer opportunities (also the ones that were already shown before):
-----
how-can-i-help --old --show newcomer
-----

The following types can be used:
wnpp, newcomer, help, no-testing, testing-autorm, rfs

Specific WNPP types:
O, RFA, RFH, ITA

And special types:
pseudo-package

IGNORE SELECTED TYPES OF OPPORTUNITIES
--------------------------------------
*how-can-i-help* can also ignore selected types of opportunities. These are
listed in ~/.config/how-can-i-help/ignored separated by whitespaces or newlines.

Allowed types:
wnpp, newcomer, help, no-testing, testing-autorm, rfs

Allowed WNPP types:
O, RFA, RFH, ITA

Allowed special types:
pseudo-package

Running with '--show' option will override "ignored types" configuration.

TYPES OF OPPORTUNITIES
----------------------
The following acronyms are used to describe the supported types:

*wnpp*
  Work-Needing and Prospective Packages.

*newcomer*
  Bugs tagged with the 'newcomer' tag. Those bugs were formerly tagged with the now deprecated 'gift' tag.

*help*
  Bugs tagged with the 'help' tag.

*no-testing*
  Packages removed from Debian 'testing'.

*testing-autorm*
  Packages going to be removed from Debian 'testing'.

*rfs*
  Request For Sponsorship.

*O*
  Orphaned.

*RFA*
  Request For Adoption.

*RFH*
  Request For Help.

*ITA*
  Intent To Adopt.

*pseudo-package*
  Newcomer bugs affecting Debian infrastructure (general Debian services). Those pseudo-packages cannot be installed.

ADDITIONAL PACKAGES
-------------------
*how-can-i-help* can also monitor packages not installed locally. These are
listed in ~/.config/how-can-i-help/packages separated by whitespaces or
newlines.

For example one can monitor all package from a server as follow:

  # ssh myserver dpkg-query -W -f \''${Package}\n'\' \
      >> ~/.config/how-can-i-help/packages

SEE ALSO
--------
https://wiki.debian.org/how-can-i-help

https://wiki.debian.org/qa.debian.org/GiftTag

https://www.debian.org/devel/wnpp

AUTHORS
-------
Tomasz Nitecki (tnnn@tnnn.pl)

Lucas Nussbaum (lucas@debian.org)

DATE
----
2016-03-03

// vim: set filetype=asciidoc:
